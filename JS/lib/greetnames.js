if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'greetnames'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'greetnames'.");
}
var greetnames = function (_, Kotlin) {
  'use strict';
  var joinToString = Kotlin.kotlin.collections.joinToString_fmv235$;
  var listOf = Kotlin.kotlin.collections.listOf_i5x0yv$;
  var println = Kotlin.kotlin.io.println_s8jyv4$;
  function greetNames(names) {
    return 'Hello ' + joinToString(names, ', ');
  }
  function main() {
    println(greetNames(listOf(['Jack', 'Jill'])));
  }
  var package$me = _.me || (_.me = {});
  var package$d3249 = package$me.d3249 || (package$me.d3249 = {});
  var package$learn = package$d3249.learn || (package$d3249.learn = {});
  var package$kotlin = package$learn.kotlin || (package$learn.kotlin = {});
  package$kotlin.greetNames_mhpeer$ = greetNames;
  package$kotlin.main = main;
  main();
  Kotlin.defineModule('greetnames', _);
  return _;
}(typeof greetnames === 'undefined' ? {} : greetnames, kotlin);
