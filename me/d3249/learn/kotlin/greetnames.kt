package me.d3249.learn.kotlin

fun greetNames(names:List<String>) = "Hello ${names.joinToString(", ")}"

fun main(){
    println(greetNames(listOf("Jack", "Jill")))
}
