### Instructions to run JS code

In order to execute JS generated code, it's required to unzip the content of kotlin-stdlib-js.jar

``` bash
  unzip $KOTLIN_HOME/lib/kotlin-stdlib-js.jar
```

Then, execute in node

``` bash
  cd JS
  node node.js
```
